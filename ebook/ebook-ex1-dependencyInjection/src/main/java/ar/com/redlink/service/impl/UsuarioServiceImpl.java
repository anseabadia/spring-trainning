package ar.com.redlink.service.impl;

import java.util.List;

import ar.com.redlink.model.Usuario;
import ar.com.redlink.persistence.EBookDAO;
import ar.com.redlink.service.UsuarioService;

public class UsuarioServiceImpl implements UsuarioService {

	private EBookDAO bookDAO;

	@Override
	@SuppressWarnings("unchecked")
	public List<Usuario> getUsuarios() {
		List<Usuario> usuarios = (List<Usuario>) bookDAO.list(Usuario.class);
		return usuarios;
	}

	@Override
	public void saveUsuario(Usuario usuario) {
		bookDAO.save(usuario);

	}

	public void setBookDAO(EBookDAO bookDAO) {
		this.bookDAO = bookDAO;
	}

}
