package ar.com.redlink.model;

import java.util.List;

public class Usuario implements PersistenObject {

	private Integer id;
	private String nombre;
	private String apellido;
	private String direccion;
	private String telefono;
	private String email;
	private List<Factura> facturas;

	public Usuario() {
	}

	public Usuario(String nombre) {
		this.nombre = nombre;
	}
	
	public String getNombre() {
		return nombre;
	}

	public String getApellido() {
		return apellido;
	}
	
	public String getDireccion() {
		return direccion;
	}
	
	public String getTelefono() {
		return telefono;
	}
	
	public String getEmail() {
		return email;
	}
	
	public Integer getId() {
		return id;
	}
	
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}
	
	public void setApellido(String apellido) {
		this.apellido = apellido;
	}
	
	public void setDireccion(String direccion) {
		this.direccion = direccion;
	}
	
	public void setTelefono(String telefono) {
		this.telefono = telefono;
	}
	
	public void setEmail(String email) {
		this.email = email;
	}
	
	public void setId(Integer iD) {
		id = iD;
	}
	
	public List<Factura> getFacturas() {
		return facturas;
	}
	
	public void setFacturas(List<Factura> facturas) {
		this.facturas = facturas;
	}
}
