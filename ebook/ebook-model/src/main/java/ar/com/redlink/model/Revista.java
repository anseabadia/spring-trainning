package ar.com.redlink.model;

public class Revista extends Producto {

	Integer numeroDeEdicion;
	
	public Revista() {
		numeroDeEdicion = 0;
	}
	
	public Integer getNumeroDeEdicion() {
		return numeroDeEdicion;
	}
	public void setNumeroDeEdicion(Integer numeroDeEdicion) {
		this.numeroDeEdicion = numeroDeEdicion;
	}

}
