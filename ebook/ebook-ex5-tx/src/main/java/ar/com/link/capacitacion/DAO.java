package ar.com.link.capacitacion;

import java.util.List;

public interface DAO {

	List<String> getValues();
	
	
	void save(String value);
	
	
}
