package ar.com.redlink.service.impl;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.transaction.TransactionConfiguration;
import org.springframework.transaction.annotation.Transactional;

import ar.com.link.capacitacion.ComponenteA;


@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration("classpath:/spring/beans_*.xml")


public class LyfecycleTest {

	@Autowired
	ApplicationContext applicationContext;
	
	@Autowired
	ComponenteA componenteA;

	@Before
	public void setUp() {
		applicationContext = null;
	}
	
//	@Test
//	public void testAfterSetProperties() {
//		
//		//TODO configurar el bean componente.a para que luego de su instanciación, setee en la propiedad numero el valor 10
//		Assert.assertEquals(10L, componenteA.getNumero());
//		
//	}

	@Test
	public void testPropertiesPlaceHolder() {
		
		//TODO Inyectar a la propiedad texto un valor obtenido desde un archivo de propiedades.
		Assert.assertEquals("localhost:3306", componenteA.getTexto());

	}
	


	
}
