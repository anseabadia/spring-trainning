package ar.com.redlink.model;

public class Libro extends Producto {

	private String autor;
	private String editorial;
	private Integer isbn;
	private String genero;
	
	public Libro() {
		autor = null;
		editorial  = null;
		isbn = null;
		genero = null;
	}
	
	public String getAutor() {
		return autor;
	}
	public void setAutor(String autor) {
		this.autor = autor;
	}
	
	public String getEditorial() {
		return editorial;
	}
	public void setEditorial(String editorial) {
		this.editorial = editorial;
	}
	
	public Integer getIsbn() {
		return isbn;
	}
	public void setIsbn(Integer isbn) {
		this.isbn = isbn;
	}
	
	public String getGenero() {
		return genero;
	}
	public void setGenero(String genero) {
		this.genero = genero;
	}

}
