package ar.com.link.capacitacion;

public interface Service {
	
	void save(Object value);
	
	Object getValue();
}
