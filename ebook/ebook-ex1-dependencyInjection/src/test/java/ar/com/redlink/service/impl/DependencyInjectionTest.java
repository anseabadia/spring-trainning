package ar.com.redlink.service.impl;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;


import ar.com.link.A;
import ar.com.link.B;
import ar.com.link.C;
import ar.com.redlink.service.UsuarioService;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration("classpath:/spring/beans-*.xml")
public class DependencyInjectionTest {

	@Autowired
	ApplicationContext applicationContext;

	@Test
	public void testDI2() {
		
		A a = (A) applicationContext.getBean("a");
		B b = (B) applicationContext.getBean("b");
		C c = (C) applicationContext.getBean("c");

		Assert.assertEquals(b, a.getB());
		Assert.assertEquals(c, a.getB().getC());

	}

	
	

	@Test
	public void testDI() {
		
		//TODO: Ejecutar satisfactoriamente el test de unidad
		
		UsuarioService usuarioService = (UsuarioService) applicationContext.getBean("usuario.service");

		Assert.assertEquals(2, usuarioService.getUsuarios().size());

	}
	
	

}
