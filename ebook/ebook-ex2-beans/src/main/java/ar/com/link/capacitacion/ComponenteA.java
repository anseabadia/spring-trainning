package ar.com.link.capacitacion;

import java.util.List;
import java.util.Set;

public class ComponenteA {

	//TODO: Completar la clase para poder realizar las inyecciones 
	private IComponenteB componenteB;

	private List values;
	
	private long numero;
	
	private Set<IComponenteB> componentesB;
	
	
	public ComponenteA(IComponenteB componenteB) {
		this.componenteB = componenteB;
	}

	public IComponenteB getComponenteB() {
		return componenteB;
	}

	public List getValues() {
		return values;
	}

	public Set<IComponenteB> getComponentesB() {
		return componentesB;
	}
	
	public long getNumero() {
		return numero;
	}

	public void setComponenteB(IComponenteB componenteB) {
		this.componenteB = componenteB;
	}

	public void setValues(List values) {
		this.values = values;
	}

	public void setNumero(long numero) {
		this.numero = numero;
	}

	public void setComponentesB(Set<IComponenteB> componentesB) {
		this.componentesB = componentesB;
	}
	
	
	
	
	
	
	

}
