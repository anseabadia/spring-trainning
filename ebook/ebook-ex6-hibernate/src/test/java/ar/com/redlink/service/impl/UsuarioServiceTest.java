package ar.com.redlink.service.impl;

import java.util.List;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.transaction.TransactionConfiguration;
import org.springframework.transaction.annotation.Transactional;

import ar.com.redlink.model.Usuario;
import ar.com.redlink.service.UsuarioService;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration("classpath:/spring/beans-*.xml")
@TransactionConfiguration
@Transactional
public class UsuarioServiceTest {

	@Autowired
	UsuarioService usuarioService;

	// TODO: Ejecutar los test satisfactoriamente.
	@Test
	public void testGetUsuarios() {
		Usuario usuario1 = new Usuario();
		usuario1.setNombre("Juan");

		Usuario usuario2 = new Usuario();
		usuario2.setNombre("Pedro");

		Usuario usuario3 = new Usuario();
		usuario3.setNombre("Martín");

		usuarioService.saveUsuario(usuario1);
		usuarioService.saveUsuario(usuario2);
		usuarioService.saveUsuario(usuario3);

		List<Usuario> usuarios = usuarioService.getUsuarios();

		Assert.assertEquals(3, usuarios.size());

	}

	@Test
	public void testSaveUsuario() {

		Usuario usuario1 = new Usuario();
		usuario1.setNombre("Pablo");

		usuarioService.saveUsuario(usuario1);

		List<Usuario> usuarios = usuarioService.getUsuarios();

		Assert.assertEquals("Pablo", usuarios.get(0).getNombre());

	}

}
