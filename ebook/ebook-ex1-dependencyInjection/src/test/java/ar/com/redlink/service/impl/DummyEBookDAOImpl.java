

package ar.com.redlink.service.impl;

import java.util.Arrays;
import java.util.List;

import ar.com.redlink.model.PersistenObject;
import ar.com.redlink.model.Usuario;
import ar.com.redlink.persistence.EBookDAO;

public class DummyEBookDAOImpl implements EBookDAO {

	@Override
	public void save(PersistenObject persistenObject) {

	}

	@Override
	public List<? extends PersistenObject> list(
			Class<? extends PersistenObject> clazz) {

		return Arrays.asList(new Usuario("Juan"), new Usuario("Pedro"));
	}

}
