package ar.com.redlink.proxy;

import org.aopalliance.intercept.MethodInterceptor;
import org.aopalliance.intercept.MethodInvocation;

public class LogInterceptor implements MethodInterceptor {

	@Override
	public Object invoke(MethodInvocation invocation) throws Throwable {
		System.out.println("Antes");
		Object result = null;
		try{
			result = invocation.proceed();
		}finally{
			System.out.println("Después");
		}
		return result;
	}

}
