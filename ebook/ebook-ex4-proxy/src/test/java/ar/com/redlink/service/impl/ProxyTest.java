package ar.com.redlink.service.impl;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import ar.com.link.capacitacion.Service;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration("classpath:/spring/beans-*.xml")
public class ProxyTest {
	@Autowired
	ApplicationContext applicationContext;

	@Test
	public void testProxy() {

		// TODO: Ejecutar el test de Beans

		Service service = (Service) applicationContext.getBean("service");

		service.save(null);

		try {
			service.getValue();
		} catch (ArithmeticException e) {
			Assert.fail();
		} catch (RuntimeException e) {
		}

	}

}
