package ar.com.link.capacitacion;

import java.util.List;

public interface Service {

	void save(String value);

	List<String> getValues();
	
}
