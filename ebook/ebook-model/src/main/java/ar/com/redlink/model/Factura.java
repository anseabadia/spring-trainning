package ar.com.redlink.model;

import java.util.Date;
import java.util.List;

public class Factura implements PersistenObject {

	Integer numero;
	Date fechaEmision;
	Double valorTotal;
	List<Item> items;
	Usuario usuario;

	public Factura() {
		numero = 0;;
		fechaEmision=null;;
		valorTotal = 0.00;
		usuario = null;
	}
	
	public Integer getNumero() {
		return numero;
	}
	
	public Date getFechaEmision() {
		return fechaEmision;
	}
	
	public Double getValorTotal() {
		return valorTotal;
	}
	
	public void setNumero(Integer numero) {
		this.numero = numero;
	}
	
	public void setFechaEmision(Date fechaEmision) {
		this.fechaEmision = fechaEmision;
	}
	
	public void setValorTotal(Double valorTotal) {
		this.valorTotal = valorTotal;
	}
	
	public List<Item> getItems() {
		return items;
	}
	
	public void setItems(List<Item> items) {
		this.items = items;
	}
	
	public Usuario getUsuario() {
		return usuario;
	}
	
	public void setUsuario(Usuario usuario) {
		this.usuario = usuario;
	}
	
	@Override
	public Integer getId() {
		return numero;
	}
	@Override
	public void setId(Integer id) {
		numero=id;
	}
}
