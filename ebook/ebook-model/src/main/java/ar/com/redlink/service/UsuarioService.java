package ar.com.redlink.service;

import java.util.List;

import javax.jws.WebService;

import ar.com.redlink.model.Usuario;

@WebService
public interface UsuarioService {

	List<Usuario> getUsuarios();

	void saveUsuario(Usuario usuario);

}
