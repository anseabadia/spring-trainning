package ar.com.link.capacitacion;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;

public class DAOImpl implements DAO {

	private JdbcTemplate jdbcTemplate;

	@Override
	public List<String> getValues() {
		return getJdbcTemplate().query("select * from value", new RowMapper() {

			@Override
			public Object mapRow(ResultSet rs, int rowNum) throws SQLException {
				return rs.getString(2);
			}

		});
	}

	public JdbcTemplate getJdbcTemplate() {
		return jdbcTemplate;
	}

	public void setJdbcTemplate(JdbcTemplate jdbcTemplate) {
		this.jdbcTemplate = jdbcTemplate;
	}

	@Override
	public void save(String value) {
		String sql = "INSERT INTO value " + "(value) VALUES (?)";

		jdbcTemplate.update(sql, new Object[] { value });

	}

}
