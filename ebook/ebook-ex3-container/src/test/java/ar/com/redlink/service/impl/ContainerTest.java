package ar.com.redlink.service.impl;

import org.junit.Before;
import org.junit.Test;
import org.springframework.context.ApplicationContext;

public class ContainerTest {

	ApplicationContext applicationContext;

	@Before
	public void setUp() {
		applicationContext = null;
	}
	@Test
	public void testClassPathApplicationContext() {
		// TODO Levantar un applicationContext haciendo referencia a los
		// archivos de beans.

	}

	@Test
	public void testImports() {
		// TODO Levantar un applicationContext haciendo referencia solo al
		// archivo beans.xml y usando en el mismo la sentencia import.

	}

	@Test
	public void testWildCards() {
		// TODO Levantar un applicationContext haciando referencia a los
		// archivos a través del uso del Wildcards.

	}

}
