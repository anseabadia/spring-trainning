package ar.com.redlink.model;

import java.util.List;

public class Carrito {

	List<Producto> producto;
	Usuario usuario;
	
	public Carrito() {
		producto = null;
		usuario = null;
	}
	
	public List<Producto> getProducto() {
		return producto;
	}
	public void setProducto(List<Producto> producto) {
		this.producto = producto;
	}
	
	public Usuario getUsuario() {
		return usuario;
	}
	public void setUsuario(Usuario usuario) {
		this.usuario = usuario;
	}

}
