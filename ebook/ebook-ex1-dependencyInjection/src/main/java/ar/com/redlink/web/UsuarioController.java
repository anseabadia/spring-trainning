package ar.com.redlink.web;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import ar.com.redlink.model.Usuario;
import ar.com.redlink.service.UsuarioService;

@Controller

public class UsuarioController {

	private UsuarioService usuarioService;

	@RequestMapping(method = RequestMethod.GET, value = "/usuarios")
	public String getUsuarios(Model model) {

		List<Usuario> usuarios = usuarioService.getUsuarios();
		model.addAttribute("usuarios", usuarios);

		return "usuarios";

	}

	@RequestMapping(method = RequestMethod.POST, value = "/saveUsuario")
	
	public String saveUsuarioUsuario(@ModelAttribute Usuario usuario) {

		usuarioService.saveUsuario(usuario);

		return "redirect:/spring/usuarios";

	}
	
	
	@RequestMapping(method = RequestMethod.GET, value = "/registrarse")
	public String registerUser() {
		return "registrarse";

	}


	@Autowired
	public void setUsuarioService(UsuarioService usuarioService) {
		this.usuarioService = usuarioService;
	}

}
