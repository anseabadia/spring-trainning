package ar.com.redlink.service.impl;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import ar.com.link.capacitacion.ComponenteA;
import ar.com.link.capacitacion.ComponenteB;
import ar.com.link.capacitacion.IComponenteB;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration("classpath:/spring/beans-*.xml")
public class BeansTest {

	@Autowired
	ApplicationContext applicationContext;

	
	@Test
	public void testFactoryMethod() {
		
		// TODO: Ejecutar el test de factoryMethod correctamente.
		IComponenteB cb = (IComponenteB) applicationContext
				.getBean("componente.b");

	}
	
	@Test
	public void testConstructor() {
		// TODO: Ejecutar el test de construcción de un bean correctamente.
		ComponenteA ca = (ComponenteA) applicationContext
				.getBean("componente.a");
		
		Assert.assertNotNull(ca.getComponenteB());
		Assert.assertEquals(1, ca.getComponentesB().size());
		Assert.assertNotNull(ca.getValues());
		Assert.assertEquals(10L, ca.getNumero());

	}


	@Test
	public void testAlias() {

		// TODO: Ejecutar el test de alias satisfactoriamente.
		IComponenteB cb1 = (IComponenteB) applicationContext
				.getBean("componente.b");
		IComponenteB cb2 = (IComponenteB) applicationContext
				.getBean("componenteB");

		Assert.assertSame(cb1, cb2);
	}

	@Test
	public void testTestScope() {

		// TODO: Ejecutar el test de scopes satisfactoriamente.

		ComponenteA ca1 = (ComponenteA) applicationContext
				.getBean("componente.a");
		ComponenteA ca2 = (ComponenteA) applicationContext
				.getBean("componente.a");

		Assert.assertNotSame(ca1, ca2);

		IComponenteB cb1 = (IComponenteB) applicationContext
				.getBean("componente.b");
		IComponenteB cb2 = (IComponenteB) applicationContext
				.getBean("componente.b");

		Assert.assertSame(cb1, cb2);

	}
	
	@Test
	public void testProxy() {

		// TODO: Ejecutar el test de scopes satisfactoriamente.



		IComponenteB cb1 = (IComponenteB) applicationContext
				.getBean("componente.b");


		cb1.example();

	}

}
