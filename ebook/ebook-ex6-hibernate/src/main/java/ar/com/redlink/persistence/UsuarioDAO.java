package ar.com.redlink.persistence;

import java.util.List;

import ar.com.redlink.model.Usuario;

public interface UsuarioDAO {

	List<Usuario> list();

	void save(Usuario usuario);

}
