package ar.com.link.capacitacion;

import java.util.List;

public class ServiceImpl implements Service {

	private DAO dao;
	
//	private Service2 service2;

	public void save(String value) {
		dao.save(value);
//		service2.save("La PLata");

	}
	

	@Override
	public List<String> getValues() {
		return dao.getValues();
	}

	public void setDao(DAO dao) {
		this.dao = dao;
	}

}
