import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;


public class JUnitTest {

	
	@Before
	public void setUp() throws Exception {
		System.out.println("Se ejecuta antes de cada Test");
	}

	@After
	public void tearDown() throws Exception {
		System.out.println("Se ejecuta después de cada Test");
	}

	@Test
	public void test() {
		System.out.println("Ejecución del Test 1");
		Assert.assertEquals(1, 1);
	}
	
	@Test
	public void test_2() {
		System.out.println("Ejecución del Test 2");
		Assert.assertEquals(1, 1);
	}


}
