package ar.com.redlink.persistence;

import java.util.List;

import ar.com.redlink.model.PersistenObject;

public interface EBookDAO {

	void save(PersistenObject persistenObject);

	List<? extends PersistenObject> list(Class<? extends PersistenObject> clazz);
}
