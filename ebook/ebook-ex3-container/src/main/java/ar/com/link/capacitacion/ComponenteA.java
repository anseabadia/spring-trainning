package ar.com.link.capacitacion;

import java.util.List;
import java.util.Set;

public class ComponenteA {

	//TODO: Completar la clase para poder realizar las inyecciones 
	private ComponenteB componenteB;

	private long numero;
	
	private String texto;
	
	private Set<ComponenteB> componentesB;
	
	public ComponenteA(ComponenteB componenteB) {
		this.componenteB = componenteB;
	}

	public ComponenteB getComponenteB() {
		return componenteB;
	}


	public Set<ComponenteB> getComponentesB() {
		return componentesB;
	}
	
	public long getNumero() {
		return numero;
	}
	
	public String getTexto() {
		return texto;
	}
	
	public void setTexto(String texto) {
		this.texto = texto;
	}
	
	
	
	
	
	
	

}
