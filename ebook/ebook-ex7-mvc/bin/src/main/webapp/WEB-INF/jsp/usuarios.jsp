<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<html>
<head>
<title>Usuarios registrados</title>
</head>
<body>

	<a href='<c:url value="/spring/registrarse"/>'>Registrar usuario</a>
	<br/>

	<table>
		<thead>
			<tr>
				<th>Id</th>
				<th>Nombre</th>
				<th>Apellido</th>
				<th>Direccion</th>
				<th>Telefono</th>
				<th>Email</th>
			</tr>
		</thead>
		<tbody>
			<c:forEach var="usuario" items="${usuarios}">
				<tr>
					<td>${usuario.id}</td>
					<td>${usuario.nombre}</td>
					<td>${usuario.apellido}</td>
					<td>${usuario.direccion}</td>
					<td>${usuario.telefono}</td>
					<td>${usuario.email}</td>
				</tr>
			</c:forEach>
		</tbody>
	</table>
</body>
</html>