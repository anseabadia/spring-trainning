package ar.com.link.capacitacion;

public class ComponenteB implements IComponenteB {

	private ComponenteB() {
	}

	public static ComponenteB createComponentB() {
		System.out.println("Hola");
		return new ComponenteB();
	}
	
	public void example(){
		System.out.println("Método del target");
	}

}
