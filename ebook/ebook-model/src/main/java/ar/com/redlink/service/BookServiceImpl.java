package ar.com.redlink.service;

import java.util.Arrays;
import java.util.List;

import org.springframework.stereotype.Service;

import ar.com.redlink.model.Libro;

public class BookServiceImpl implements BookService {

	@Override
	public List<Libro> getLibros(int cantidad) {
		
		Libro l = new Libro();
		l.setTitulo("Libro1");
		
		Libro l2 = new Libro();
		l2.setTitulo("Libro2");
		
		
		return Arrays.asList(l, l2);
	}

}
