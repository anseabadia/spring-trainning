package ar.com.redlink.model;

public class Item implements PersistenObject {

	Integer id;
	Integer cantidad;
	Double valorUnitario;
	Factura factura;
	Producto producto;

	public Item() {
		cantidad = 0;
		valorUnitario = 0.00;
		factura = null;
	}
	
	public Integer getCantidad() {
		return cantidad;
	}
	
	public Double getValorUnitario() {
		return valorUnitario;
	}
	
	public void setCantidad(Integer cantidad) {
		this.cantidad = cantidad;
	}
	
	public void setValorUnitario(Double valorUnitario) {
		this.valorUnitario = valorUnitario;
	}
	
	public Factura getFactura() {
		return factura;
	}
	
	public void setFactura(Factura factura) {
		this.factura = factura;
	}

	@Override
	public Integer getId() {
		return id;
	}
	
	@Override
	public void setId(Integer id) {
		this.id = id;
	}
}
