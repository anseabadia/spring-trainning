package ar.com.redlink.service.impl;

import java.util.List;

import ar.com.redlink.model.Usuario;
import ar.com.redlink.persistence.UsuarioDAO;
import ar.com.redlink.service.UsuarioService;

public class UsuarioServiceImpl implements UsuarioService {

	private UsuarioDAO usuarioDAO;

	@Override
	public List<Usuario> getUsuarios() {
		List<Usuario> usuarios = (List<Usuario>) usuarioDAO.list();
		return usuarios;
	}

	@Override
	public void saveUsuario(Usuario usuario) {
		if(usuario.getNombre() == null){
			throw new RuntimeException("El nombre no puede ser nulo");
		}
		usuarioDAO.save(usuario);

	}

	public void setUsuarioDAO(UsuarioDAO usuarioDAO) {
		this.usuarioDAO = usuarioDAO;
	}

}
