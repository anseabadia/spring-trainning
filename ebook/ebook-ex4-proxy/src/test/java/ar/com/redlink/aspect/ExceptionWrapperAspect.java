package ar.com.redlink.aspect;

public class ExceptionWrapperAspect {

	// TODO: Configurar para que ante cada excepción lanzada se wrapee en una
	// Runtime.
	public void wrapException(Throwable throwable) {

		throw new RuntimeException(throwable.getMessage());
	}

}
