package ar.com.redlink.service.impl;

import java.util.List;

import ar.com.redlink.model.Usuario;
import ar.com.redlink.persistence.EBookDAO;
import ar.com.redlink.service.UsuarioService;

public class DummyUsuarioServiceImpl implements UsuarioService {

	//TODO: Inyectar en esta propiedad el EBookDAO
	private EBookDAO bookDAO;
	
	@SuppressWarnings("unchecked")
	@Override
	public List<Usuario> getUsuarios() {
		return (List<Usuario>) bookDAO.list(Usuario.class);
	}

	
	@Override
	public void saveUsuario(Usuario usuario) {
	}

	
	public void setBookDAO(EBookDAO bookDAO) {
		this.bookDAO = bookDAO;
	}
	

}
