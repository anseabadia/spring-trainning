package ar.com.redlink.persistence.impl;

import java.util.List;

import org.springframework.orm.hibernate3.HibernateTemplate;

import ar.com.redlink.model.PersistenObject;
import ar.com.redlink.persistence.EBookDAO;

public class EBookDAOImpl implements EBookDAO{

	private HibernateTemplate hibernateTemplate;

	public void setHibernateTemplate(HibernateTemplate hibernateTemplate) {
		this.hibernateTemplate = hibernateTemplate;
	}

	@Override
	public void save(PersistenObject persistenObject) {
		hibernateTemplate.save(persistenObject);
		
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<? extends PersistenObject> list(Class<? extends PersistenObject> clazz) {
		return hibernateTemplate.find("from " + clazz.getName());
		
	}
	
	
	
	
	
	
	
}
