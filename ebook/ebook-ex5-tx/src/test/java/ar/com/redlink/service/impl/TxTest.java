package ar.com.redlink.service.impl;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.transaction.TransactionConfiguration;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import ar.com.link.capacitacion.Service;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration("classpath:/spring/beans-*.xml")

@TransactionConfiguration( defaultRollback=false, transactionManager = "transaction.manager")
public class TxTest {
	@Autowired
	ApplicationContext applicationContext;

	@Test
	@Transactional(  rollbackFor = RuntimeException.class, propagation=Propagation.REQUIRED)
	public void testTx() {

		// TODO: Ejecutar el test de Beans

		Service service = (Service) applicationContext.getBean("service");

		service.save("Ejemplo-233");
		service.save("Ejemplo-432");
		service.save("Ejemplo-345");
		
		throw new RuntimeException();

		// List<String> values = service.getValues();

		// Assert.assertTrue(values.contains("Ejemplo-1"));
		// Assert.assertTrue(values.contains("Ejemplo-2"));
		// Assert.assertTrue(values.contains("Ejemplo-3"));

	}

}
