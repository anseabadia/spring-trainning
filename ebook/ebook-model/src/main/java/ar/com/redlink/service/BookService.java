package ar.com.redlink.service;

import java.util.List;

import javax.jws.WebService;

import ar.com.redlink.model.Libro;
import ar.com.redlink.model.Usuario;

@WebService
public interface BookService {

	List<Libro> getLibros(int cantidad);


}
